//app.js
App({
  httpurl: 'https://xcx.xaxy.cc/shop/',
  storage_pre: 'xyshop_',
  onLaunch:function()
  {
    var that = this;
    // 判断session_key是否有效
    wx.checkSession({
      success () {
        //session_key 未过期，并且在本生命周期一直有效
        console.log('session_key未失效')
      },
      fail () {
        // session_key 已经失效，需要重新执行登录流程
        wx.login({
          success(res) {
            if (res.code) {
              // 发起网络请求
              wx.request({
                url: that.httpurl+'index/index/xcxLogin',
                header: {
                  'content-type': 'application/json' // 默认值
                },
                data: {
                  code: res.code
                },
                method:'POST',
                success (res) {
                  var data_info = res.data.data;
                  var user_info_key = that.storage_pre+'user_session';
                  wx.setStorageSync(user_info_key,data_info);
                }
              })
            } else {
              console.log('登录失败！' + res.errMsg)
            }
          }
        })
      }
    })
  }
})