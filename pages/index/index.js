//index.js
//获取应用实例
const app = getApp()
import Toast from '../../miniprogram/miniprogram_npm/@vant/weapp/toast/toast'
Page({
  data: {
    Searchvalue: '',
    imgUrls: [
        {id:1,url:'/images/swiper/1.jpg'}, 
        {id:2,url:'/images/swiper/2.jpg'},
        {id:3,url: '/images/swiper/3.jpg'}
      ],
    goodsList:[
      { id: 1, goodsname: '中国好声音', goodsurl:'/images/goods/1.jpg',goodsprize:'12.36',goodsnum:'232'},
      { id: 2, goodsname: '中国好声音2', goodsurl: '/images/goods/2.jpg', goodsprize: '16.36', goodsnum: '23241' },
      { id: 3, goodsname: '中国好声音3', goodsurl: '/images/goods/3.jpg', goodsprize: '12.39', goodsnum: '2321' }
    ],
    height:0
  },
  onChange(e) {
    this.setData({Searchvalue: e.detail});
  },
  onSearch() {
    Toast('搜索' + this.data.Searchvalue);
  },
  addCar(e)
  {
    /*商品编号*/
    let goods_sn = e.target.dataset.goods_sn;
    wx.showModal({ title: '添加成功！', content:'去富矿'});
  },
  //设置轮播容器的高度
  setContainerHeight: function (e) {

    //图片的原始宽度
    var imgWidth = e.detail.width;

    //图片的原始高度
    var imgHeight = e.detail.height;

    //同步获取设备宽度
    var sysInfo = wx.getSystemInfoSync();

    //获取屏幕的宽度
    var screenWidth = sysInfo.screenWidth;

    //获取屏幕和原图的比例
    var scale = screenWidth / imgWidth;

    //设置容器的高度
    this.setData({
      height: imgHeight * scale
    })
  }
})
