// pages/user/user.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hasUserInfo:false,
    userInfo:{},
    user_session:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var openid_key = app.storage_pre+'user_session';
    var usersession = wx.getStorageSync(openid_key);
    this.setData({
      user_session:usersession
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getUserProfile(e)
  {
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        });
        // 将加密数据提交后台
        var that = this;
        wx.request({
          url: app.httpurl+'index/index/userInfo', //仅为示例，并非真实的接口地址
          data: {
            openid: that.data.user_session['openid'],
            token: that.data.user_session['token'],
            session_key: that.data.user_session['session_key'],
            rawData:res.rawData,
            signature:res.signature,
            encryptedData:res.encryptedData,
            iv:res.iv
          },
          header: {
            'content-type': 'application/json' // 默认值
          },
          method:'POST',
          success (res) {
            console.log(res.data)
          }
        });
      }
    })
  }
})